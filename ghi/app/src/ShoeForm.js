import React, { useEffect, useState } from "react";

function ShoeForm(){

  const [bins, setBins] = useState([])

  const [manufacturer, setManufacturer] = useState('');
  const [modelName, setModelName] = useState('');
  const [color, setColor] = useState('');
  const [pictureURL, setPictureURL] = useState('');
  const [bin, setBin] = useState('');

  const handleChangeManufacturer = (event) => {
    const value = event.target.value;
    setManufacturer(value);
  }
  const handleChangeModelName = (event) => {
    const value = event.target.value;
    setModelName(value);
  }
  const handleChangeColor = (event) => {
    const value = event.target.value;
    setColor(value);
  }
  const handleChangePictureURL = (event) => {
    const value = event.target.value;
    setPictureURL(value);
  }
  const handleChangeBin = (event) => {
    const value = event.target.value;
    setBin(value);
  }

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.manufacturer = manufacturer;
    data.model_name = modelName;
    data.color = color;
    data.url = pictureURL;
    data.bin = bin;


    const shoesURL = 'http://localhost:8080/api/shoes/';
    const fetchOptions = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const shoesResposne = await fetch(shoesURL, fetchOptions);
    if (shoesResposne.ok) {
      const newshoes = await shoesResposne.json();
      setManufacturer('');
      setModelName('');
      setColor('');
      setPictureURL('');
      setBin('');
    }
  }

  const fetchData = async() => {
    const url = 'http://localhost:8100/api/bins/'
    const resposne = await fetch(url);
    if (resposne.ok) {
      const data = await resposne.json();
      setBins(data.bins);
    }
  }

  useEffect(() =>{
    fetchData();
  }, []);

  return(
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add Shoes</h1>
          <form onSubmit={handleSubmit} id="create-shoe-form">
            <div className="form-floating mb-3">
              <input onChange={handleChangeManufacturer} value={manufacturer} placeholder="Manufacturer" required name="manufacturer" type="text" id="manufacturer" className="form-control" />
              <label htmlFor="manufacturer">Manufacturer</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleChangeModelName} value={modelName} placeholder="Model Name" required name="model_name" type="text" id="model_name" className="form-control" />
              <label htmlFor="model_name">Model Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleChangeColor} value={color} placeholder="Color" type="text" name="color" id="color" className="form-control" />
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleChangePictureURL} value={pictureURL} placeholder="Image URL" required name="url" type="url" id="url" className="form-control" />
              <label htmlFor="url">Picture URL</label>
            </div>
            <div className="mb-3">
              <select onChange={handleChangeBin} value={bin} required name="bin" className="form-select" id="bin">
                <option value="">Choose a Bin</option>
                {bins.map(bin => {
                  return (
                    <option key={bin.id} value={bin.href}>
                      {bin.closet_name}
                    </option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );

}


export default ShoeForm;
