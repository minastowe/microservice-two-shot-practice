import React from "react";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";

function ShoeList(props){
  const [shoes, setShoes] = useState([]);

  const fetchData = async() => {
    const url = 'http://localhost:8080/api/shoes/';
    const response = await fetch(url);
    if (response.ok){
      const data = await response.json();
      setShoes(data.shoes);
    }
  }

  useEffect(() =>{
    fetchData();
  }, []);

  const FunRemove=((id) => {
    if (window.confirm("Are You Sure?")){
      fetch('http://localhost:8080/api/shoes/'+id,
      {method:"DELETE"}).then(() => {
        window.location.reload();
      }).catch((err)=> {
        console.log(err.message) 
      })
    }
  })

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Manufacturer</th>
          <th>Model Name</th>
          <th>Color</th>
          <th>PlaceHolder</th>
        </tr>
      </thead>
      <tbody>
        {shoes.map(shoe => {
          return (
            <tr key={shoe.id}>
              <td>{ shoe.manufacturer }</td>
              <td>{ shoe.model_name }</td>
              <td>{ shoe.color}</td>
              <td>
                <button className="btn btn-danger" onClick={()=>{FunRemove(shoe.id)}}>Delete</button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );


}
export default ShoeList;
