import React from 'react'
import videoBG from './assets/videoWardrobeBG.mp4'
import './index.css'

function MainPage() {
  return (
    <>
      <div className="mainBG">
        <video src={videoBG} autoPlay loop muted />
      </div>
      <div className="text-container px-4 py-5 my-5 text-center">
        <div className="content" >
          <h1 className="display-5 fw-bold">WARDROBIFY!</h1>
            <div className="col-lg-7 mx-auto">
              <p className="lead mb-4">
                Need to keep track of your shoes and hats?
                We have the solution for you!
              </p>
            </div>
        </div>
      </div>
    </>
  );
}

export default MainPage;
