import { useState, useEffect } from 'react';


function HatList (){
    const [hats, setHats] = useState([]);

    async function fetchHats(){
        const response = await fetch('http://localhost:8090/api/hats/');

        if (response.ok){
            const parsedJson = await response.json();
            setHats(parsedJson.hats);
        }
    }

    const handleDelete = async (id) => {
        const url = `http://localhost:8090/api/hats/${id}`;
        const response = await fetch(url, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json'
            }
        });
    }
    useEffect(() => {
        fetchHats();
    }, []);

    return (
        <table className="table table-striped">
        <thead>
          <tr>
            <th>Fabric</th>
            <th>Style Name</th>
            <th>Color</th>
            <th>Closet Name</th>
            <th>Picture</th>
          </tr>
        </thead>
        <tbody>
          {hats.map(hat => {
            return (
              <tr key={hat.id}>
                <td>{ hat.fabric }</td>
                <td>{ hat.style_name }</td>
                <td>{ hat.color }</td>
                <td>{ hat.location.closet_name }</td>
                <td>
                    <img src={ hat.url_pic } alt={ hat.url_pic } width='150' />
                </td>
                <td><button onClick={() => handleDelete(hat.id)} className='btn btn-primary'>Delete</button></td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
}

export default HatList;