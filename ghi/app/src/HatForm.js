import React, {useEffect, useState} from 'react';

function HatForm(){
    const [locations, setLocations] = useState([]);
    const [fabric, setFabric] = useState('');
    const [styleName, setStyleName] = useState('');
    const [color, setColor] = useState('');
    const [urlPic, setUrlPic] = useState('');
    const [location, setLocation] = useState('');

    const handleFabricChange = (event) => {
        const value = event.target.value;
        setFabric(value);
    }
    
    const handleStyleNameChange = (event) => {
        const value = event.target.value;
        setStyleName(value);
    }

    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handleUrlPicChange = (event) => {
        const value = event.target.value;
        setUrlPic(value);
    }

    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }
    
    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {
            fabric: fabric,
            style_name: styleName,
            color: color,
            url_pic: urlPic,
            location: location,
        };


        const hatUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
    const response = await fetch(hatUrl, fetchConfig);
    if (response.ok){
        // const newHat = await response.json();
        // console.log(newHat);

        setFabric('');
        setStyleName('');
        setColor('');
        setUrlPic('');
        setLocation('');
    }
}
    const fetchData = async () => {
        const url = 'http://localhost:8100/api/locations/';
        const response = await fetch(url);
        if(response.ok){
            const data = await response.json();
            setLocations(data.locations);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);
    
    return(
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new hat</h1>
                    <form id="create-hat-form" onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <input value={fabric} onChange={handleFabricChange}placeholder="Fabric" required type="text" id="fabric" name="fabric" className="form-control" />
                            <label htmlFor="fabric">Fabric</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={styleName} onChange={handleStyleNameChange}placeholder="Style Name" required type="text" id="style_name" name="style_name" className="form-control" />
                            <label htmlFor="style_name">Style Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={color} onChange={handleColorChange}placeholder="Color" type="text" id="color" name="color" className="form-control" />
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={urlPic} onChange={handleUrlPicChange}placeholder="Url Picture" required type="text" id="url_pic" name="url_pic" className="form-control" />
                            <label htmlFor="url_pic">Url Picture</label>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="location" className="form-label">Location</label>
                            <select required value={location} onChange={handleLocationChange} id="location" name="location" className="form-select">
                                <option value="">Choose a location</option>
                                {locations.map(location => {
                                    return (
                                        <option key={location.href} value={location.href}>
                                            {location.closet_name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
                </div>
            </div>
            </div>
    );

}

export default HatForm;