# Wardrobify

Team:

* Person 1 - Priyanka Kannan - Shoes
* Person 2 - Mina Nguyen - Hats

## About this project
Wardobify was created for those who want to keep an organized list of their clothing items and accessories. Whether the user has a smaller wardrobe or a large one, wardrobify keeps records of where the clothing item is stored and helps every day shoppers from buying the same items twice! This application is broken down into two microservices: Shoes and Hats.

## Shoes microservice

The Shoes microservice has two parts: 'api' and 'poll.' 'api' uses Django to manage shoe data, connecting it to user wardrobes. 'poll' periodically fetches bin data. The front-end, powered by React, includes 'ShoeList,' 'ShoeForm,' and a delete feature for users to easily create, view, and remove shoes from their wardrobes.

## Hats microservice

The Hat microservice utilizes a Restful API to interact with Hat resources and the poller to poll the Wardrobe API for Location resources. React was used to create a dynamic front-end application, featuring a 'HatList' and 'HatForm' to view, create, and delete hats from the users' wardrobe. 

## Built with
* Postgres
* ReactJS
* Docker
* Django
* Bootstrap

## Getting Started
Just a few things before you dive into the project!
* Docker Desktop
* Insomnia

Installation
* Clone the repository into a directory of your choosing
* Use "docker volume build two-shot-pgdata" command to build the volume
* Use the "docker-compose build" command to build the necessary images
* Then use the "docker-compose up" command to create the containers
* Head over to http://localhost:3000/ to interact with the application!

Create Location
*  Go to Insomnia app
*  Create post with the following link: http://localhost:8100/api/locations/
*  Enter the following in a JSON format:
    {
	"closet_name": "[Random closet name here]",
	"section_number": [Random number here],
	"shelf_number": [Random number here]
    }
* Head over to your browser and create a hat! 
