from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import LocationVO, Hat
# Create your views here.


class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "import_href",
        "closet_name",
        "section_number",
        "shelf_number",
    ]


class HatEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "fabric",
        "style_name",
        "color",
        "url_pic",
        "location",
    ]
    encoders = {
        "location": LocationVODetailEncoder()
    }

# View ALL hats AND create hats
@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatEncoder,
        )
    else: # POST
        content = json.loads(request.body)
        #Get the location object and put it in the content dict
        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        hats = Hat.objects.create(**content)
        return JsonResponse(
            hats,
            encoder=HatEncoder,
            safe=False,
        )


# View hat details, update, and delete hats
@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_hats(request, pk):
    if request.method == "GET":
        try:
            hats = Hat.objects.get(id=pk)
            return JsonResponse(
                hats,
                encoder=HatEncoder,
                safe=False,
            )
        except Hat.DoesNotExist:
            response = JsonResponse({"message": "Hat does not exist"})
            response.status_code=404
            return response
    elif request.method == "DELETE":
        try:
            hats = Hat.objects.get(id=pk)
            hats.delete()
            return JsonResponse(
                hats,
                encoder=HatEncoder,
                safe=False
            )
        except Hat.DoesNotExist:
            return JsonResponse({"message": "Hat does not exist"})
    else: #PUT
        try:
            content = json.loads(request.body)
            hats = Hat.objects.get(id=pk)

            props = [
                "fabric",
                "style_name",
                "color",
                "url_pic",
                "location"
            ]
            for prop in props:
                if prop in content:
                    setattr(hats, prop, content[prop])
            hats.save()
            return JsonResponse(
                hats,
                encoder=HatEncoder,
                safe=False,
            )
        except Hat.DoesNotExist:
            response = JsonResponse({"message": "Hat does not exist"})
            response.status_code = 404
            return response